/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_chrootmanager.h>
#include <buildboxrun_userchroot.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_runner.h>
#include <buildboxrun_chrootmanager.h>

#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

namespace {

/**
 * Check Platform properties for the chroot root digest,
 * ensure that's not empty and populate output parameter 'chrootDigest'
 */
bool isMergeRequest(const Platform &platform, Digest *chrootDigest)
{
    static const char chrootPropName[] = "chrootRootDigest";
    for (const auto &property : platform.properties()) {
        if (property.name() == chrootPropName) {
            const std::string &hash = property.value();
            if (hash.empty()) {
                std::ostringstream oss;
                oss << "Found invalid '" << chrootPropName
                    << "' value with an empty hash";
                BUILDBOX_LOG_ERROR(oss.str());
                throw std::runtime_error(oss.str());
            }

            const char *slash = strchr(hash.c_str(), '/');
            if (slash == nullptr) {
                std::ostringstream oss;
                oss << "Found invalid '" << chrootPropName
                    << "' value with valid hash, but no blob size";
                BUILDBOX_LOG_ERROR(oss.str());
                throw std::runtime_error(oss.str());
            }

            chrootDigest->set_hash(
                std::string(hash.c_str(), slash - hash.c_str()));
            chrootDigest->set_size_bytes(std::stoll(slash + 1));
            return true;
        }
    }

    return false;
}

} // namespace

ActionResult UserChrootRunner::execute(const Command &command,
                                       const Digest &inputRootDigest)
{
    // Check the Platform properties to see if we need to merge
    Digest digest(inputRootDigest);
    Digest chrootDigest;
    if (isMergeRequest(command.platform(), &chrootDigest)) {
        Digest mergedDigest;
        mergeDigests(inputRootDigest, chrootDigest, &mergedDigest);
        BUILDBOX_LOG_DEBUG("inputRootDigest = "
                           << inputRootDigest
                           << ", chrootDigest = " << chrootDigest
                           << ", mergedDigest = " << mergedDigest);
        digest.Swap(&mergedDigest);
    }

    const auto stagedDir =
        this->stage(digest, this->d_stage_path, this->d_use_localcas_protocol);

    std::ostringstream workingDir;
    workingDir << stagedDir->getPath() << "/" << command.working_directory();

    ActionResult result;
    {
        createOutputDirectories(command, workingDir.str());

        // userchroot doesn't allow symlinks in the root path
        char resolved_path[PATH_MAX];
        if (realpath(stagedDir->getPath(), resolved_path) == NULL) {
            BUILDBOX_LOG_ERROR("realpath() failed for " +
                               std::string(stagedDir->getPath()) + ": " +
                               std::strerror(errno));
            throw std::system_error(errno, std::system_category());
        }

        ChrootManager userChrootMgr(d_userchroot_bin, resolved_path);
        // environment variables set in file, which is sourced in command
        const std::vector<std::string> commandLine =
            userChrootMgr.generateCommandLine(command);

        BUILDBOX_LOG_DEBUG("Executing "
                           << logging::printableCommandLine(commandLine));
        executeAndStore(commandLine, &result);
    }

    BUILDBOX_LOG_DEBUG("Capturing command outputs...");
    stagedDir->captureAllOutputs(command, &result);
    // Set permissions of the stage dir to 0777
    buildboxcommon::Runner::recursively_chmod_directories(stagedDir->getPath(),
                                                          0777);
    BUILDBOX_LOG_DEBUG("Finished capturing command outputs");
    return result;
}

void UserChrootRunner::printSpecialUsage()
{
    std::clog << "    --userchroot-bin=PATH             Path to userchroot "
                 "executable. Will default to looking in PATH if not set.\n";
}

bool UserChrootRunner::parseArg(const char *arg)
{
    assert(arg);
    const char *assign = strchr(arg, '=');

    if (arg[0] == '-' && arg[1] == '-') {
        arg += 2;
        if (assign) {
            const auto key_len = static_cast<size_t>(assign - arg);
            const char *value = assign + 1;
            if (strncmp(arg, "userchroot-bin", key_len) == 0) {
                this->d_userchroot_bin = std::string(value);
                return true;
            }
        }
    }
    return false;
}

void UserChrootRunner::mergeDigests(const Digest &inputDigest,
                                    const Digest &chrootDigest,
                                    Digest *mergedRootDigest)
{
    // acquire the input trees of both digests
    // getTree will throw on error
    MergeUtil::DirectoryTree inputTree, chrootTree;
    inputTree = this->d_casClient->getTree(inputDigest);
    chrootTree = this->d_casClient->getTree(chrootDigest);

    // merge the two trees - failure means a conflict
    // was found while doing the merge
    digest_string_map dsMap;
    const bool success = MergeUtil::createMergedDigest(
        inputTree, chrootTree, mergedRootDigest, &dsMap);
    if (!success) {
        std::ostringstream oss;
        oss << "Error merging inputDigest(" << inputDigest
            << ") with chrootDigest(" << chrootDigest << ")";
        BUILDBOX_LOG_ERROR(oss.str());
        throw std::runtime_error(oss.str());
    }

    // save the newly generated digests to casd via 'uploadBlobs' API
    std::vector<Client::UploadRequest> requests;
    for (const auto &it : dsMap) {
        requests.emplace_back(it.first, it.second);
    }
    const std::vector<Client::UploadResult> results =
        this->d_casClient->uploadBlobs(requests);

    // verify that all blobs have been successfully uploaded
    // throw on any failures
    bool uploadSuccess = true;
    std::ostringstream oss;
    for (const auto &result : results) {
        if (!result.status.ok()) {
            oss << "Failed to upload a merged digest(" << result.digest
                << "), status = [" << result.status.error_code() << ": \""
                << result.status.error_message() << "\"]\n";
            uploadSuccess = false;
        }
    }

    if (!uploadSuccess) {
        throw std::runtime_error(oss.str());
    }
}

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
