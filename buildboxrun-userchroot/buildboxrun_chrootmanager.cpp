/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_chrootmanager.h>
#include <buildboxrun_commandfilemanager.h>

#include <string>
#include <vector>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

ChrootManager::ChrootManager(const std::string &userchroot_bin,
                             const std::string &root_path)
    : d_userchroot_bin(userchroot_bin), d_root_path(root_path)
{
}

const std::vector<std::string>
ChrootManager::generateCommandLine(const Command &command)
{
    d_command_file.reset(new CommandFileManager(
        command, d_root_path, s_command_file_location, s_command_file_prefix));
    std::vector<std::string> commandLine;

    commandLine.emplace_back(d_userchroot_bin);
    commandLine.emplace_back(d_root_path);
    commandLine.emplace_back("/bin/sh");
    commandLine.emplace_back("-c");
    commandLine.emplace_back(d_command_file->getFilePathInChroot());

    return commandLine;
}

const std::string ChrootManager::s_command_file_location = "/tmp";

const std::string ChrootManager::s_command_file_prefix = "command_file_";

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
