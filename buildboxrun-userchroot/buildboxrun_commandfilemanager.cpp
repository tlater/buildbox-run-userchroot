/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_commandfilemanager.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_temporaryfile.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

namespace {

// Add proper shell quoting to string
std::string shellQuoteString(const std::string &expression)
{
    std::ostringstream replace;
    auto replaceSingleQuotes = [](const char c) {
        return (c == '\'') ? "'\"'\"'" : std::string(1, c);
    };
    std::transform(expression.begin(), expression.end(),
                   std::ostream_iterator<std::string>(replace),
                   replaceSingleQuotes);

    return "'" + replace.str() + "'";
}

// join paths and normalize
std::string normalize_and_make_absolute(const std::string &rel_path,
                                        const std::string &base_path)
{
    std::string absolute_path =
        FileUtils::make_path_absolute(rel_path, base_path);
    return FileUtils::normalize_path(absolute_path.c_str());
}

// get filename from path
std::string get_file_name(const std::string &path)
{
    std::size_t location = path.find_last_of('/');
    return path.substr(location + 1);
}

} // unnamed namespace

CommandFileManager::CommandFileManager(const Command &command,
                                       const std::string &chroot_location,
                                       const std::string &location_in_chroot,
                                       const std::string &file_prefix)
    : d_command(command)
{
    // construct temporary file
    std::string abs_dir_location =
        normalize_and_make_absolute(location_in_chroot, chroot_location);
    d_tmp_command_file = std::make_unique<TemporaryFile>(
        abs_dir_location.c_str(), file_prefix.c_str(), 0700);

    // get absolute path
    d_file_abs_path = d_tmp_command_file->name();

    // get absolute path inside chroot
    std::string file_location = get_file_name(d_file_abs_path);
    d_file_abs_path_in_chroot =
        normalize_and_make_absolute(file_location, location_in_chroot);

    // write command to file
    writeCommandToFile();

    // close file to allow execution
    d_tmp_command_file->close();
}

std::string CommandFileManager::getFilePathInChroot() const
{
    return d_file_abs_path_in_chroot;
}

std::string CommandFileManager::getAbsoluteFilePath() const
{
    return d_file_abs_path;
}

void CommandFileManager::writeCommandToFile()
{
    auto &environment = d_command.environment_variables();
    auto &arguments = d_command.arguments();
    std::string working_dir = d_command.working_directory();

    std::ofstream commandFile;
    commandFile.open(d_file_abs_path, std::ios_base::app);
    if (commandFile.fail()) {
        std::ostringstream oss;
        oss << "Failed to open file \"" << d_file_abs_path << ", errno = ["
            << errno << ":" << strerror(errno) << "]";
        BUILDBOX_LOG_ERROR(oss.str());
        throw std::runtime_error(oss.str());
    }
    commandFile << "#!/bin/sh\n";

    // add environment variables
    for (const auto &envVar : environment) {
        commandFile << "export " << envVar.name() << "="
                    << shellQuoteString(envVar.value()) << ";\n";
    }

    // change to working directory
    commandFile << "cd " << shellQuoteString("/" + working_dir) << ";\n";
    // remove file before executing command
    commandFile << "rm $0;\n";

    // add arguments
    commandFile << "exec";
    for (auto argVar = arguments.cbegin(); argVar != arguments.cend();
         ++argVar) {
        commandFile << " " << shellQuoteString(*argVar);
    }
    commandFile.close();
}

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
