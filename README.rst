What is buildbox-run-userchroot?
===============================

``buildbox-run-userchroot`` attempts to run actions within a chroot.
``buildbox-run-userchroot`` depends on userchroot being installed on the system.
Note that userchroot is an opensource tool: https://github.com/bloomberg/userchroot,
and is seperate from generic chroot-jails like the one provided by GNU coreutils.

Usage
=====
::

    usage: ./buildbox-run-userchroot [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-file=FILE             File to write log to
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=SECONDS       How long to wait between grpc retries
        --workspace-path=PATH       Path which runner will use as root when executing jobs
        --userchroot-bin=PATH       Path to userchroot executable. Defaults to searching PATH.
