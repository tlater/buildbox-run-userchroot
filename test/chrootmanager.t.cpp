/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_systemutils.h>
#include <buildboxrun_chrootmanager.h>

#include <gtest/gtest.h>
#include <string>
#include <vector>

using namespace buildboxcommon;
using namespace buildboxrun;
using namespace userchroot;

const char *USERCHROOT_BIN = "userchrootmock.sh";

TEST(ChrootManagerTest, TestGenerateCommandLine)
{
    const std::string current_working_dir =
        SystemUtils::get_current_working_directory();
    const std::string command_file_directory = current_working_dir + "/tmp";
    FileUtils::create_directory(command_file_directory.c_str());
    // make sure nothing is in directory
    EXPECT_TRUE(FileUtils::directory_is_empty(command_file_directory.c_str()));

    ChrootManager chrootManager(USERCHROOT_BIN, current_working_dir);

    Command command;
    auto result = chrootManager.generateCommandLine(command);
    std::size_t location = result[4].find_last_of("_");
    std::string unique_identifier = result[4].substr(location + 1);
    std::vector<std::string> expected{
        USERCHROOT_BIN, current_working_dir, "/bin/sh", "-c",
        "/tmp/command_file_" + unique_identifier};
    EXPECT_EQ(result, expected);
    // make sure command file has not been deleted
    EXPECT_FALSE(
        FileUtils::directory_is_empty(command_file_directory.c_str()));
    FileUtils::delete_directory(command_file_directory.c_str());
}
